source("cosxTaylor.R")

x = seq(-pi, pi)

cat("\n\n-----------------------------")

#Ex 2
#--------------------
cat("\nExercicio 2 -----------------")
cat("\np4(x) = ")
p <- taylorCosX(4)
print(p)
cat("\np10(x) = ")
p <- taylorCosX(10)
print(p)
cat("-----------------------------\n\n")
#--------------------

#Ex 3
#--------------------------------------------------------------------------------
plot(cos, -4 * pi, 4 * pi, xlim = c(-10, 10), ylim = c(-2, 2))
title(main = "Taylor cos(x)")

abline(v = 0, col = "black")
lines(taylorCosX(4), col = "green")
lines(taylorCosX(10), col = "blue")

legend("topleft", c("f(x)", "P4(x)", "P10(x)"), fill = c("blue", "red", "green"))
#--------------------------------------------------------------------------------

#Ex 4
#---------------------------------------------------
cat("\nExercicio 4 -----------------")
cat("\n")
cat(sprintf('cos(%0.1f) = %0.10f\n', 0.2, cos(0.2)))
cat("\n")
taylorAproxCosx(0.2, 4)
cat("\n")
taylorAproxCosx(0.2, 10)
cat("-----------------------------")
#---------------------------------------------------

